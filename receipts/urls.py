from django.urls import path
from receipts.views import receipt_list
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path("", login_required(receipt_list), name="home"),
]
